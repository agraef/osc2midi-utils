
# Package name and version:
dist = osc2midi-utils-$(version)
version = 0.2

# Targets to build. to2omm only requires pure and pure-xml. gosc2midi also
# needs a few additional dependencies (pure-ffi, pure-liblo, pure-sockets,
# pure-tk, gnocl, gnocl-builder, gnocl-gconf, as well as GTK2 and pkg-config);
# if you don't have these then passing 'progs=to2omm' to the 'make' command
# will let you build just to2omm.
progs = to2omm gosc2midi

# Platform-specific stuff. Only Linux and Mac OS X have been tested and are
# known to work. The platform-specific information can be obtained from
# Pure's pkg-config.
DLL         = $(shell pkg-config pure --variable DLL)
PIC         = $(shell pkg-config pure --variable PIC)
shared      = $(shell pkg-config pure --variable shared)
purelib     = $(shell pkg-config pure --variable libdir)/pure

pure_incl   = $(shell pkg-config pure --cflags)
pure_libs   = $(shell pkg-config pure --libs)

# Compilation and linker flags. Adjust these as needed.
CFLAGS = -g -O2
ALL_CFLAGS = $(PIC) $(CFLAGS) $(CPPFLAGS) -I. $(pure_incl)
ALL_LDFLAGS = $(LDFLAGS) $(pure_libs) $(LIBS)

# Default installation prefix.
prefix = /usr/local

# Helper libraries get installed into the Pure library directory.
libdir = $(purelib)
bindir = $(prefix)/bin

# Helper libraries.
solibs = decode64$(DLL) xmlzipio$(DLL)
libsrc = $(patsubst %$(DLL),%.pure,$(solibs))

# XDG installation prefix.
xdgprefix = /usr

# Install locations for the application icon and desktop file.
xdgappdir = $(xdgprefix)/share/applications
xdgicondir = $(xdgprefix)/share/pixmaps

# Program targets to build.
targets = $(addsuffix $(EXE), $(progs))

# Built targets.
built = $(foreach target,$(targets),$(wildcard $(target)))

check = $(wildcard $(purelib)/$(1)$(DLL))

ifeq ($(DLL),.dylib)
# OSX: hardcode installation path into the dlls.
dllname = -install_name "$(libdir)/$@"
# Link against the bonjour module for Zeroconf support if we have it.
extralibs = $(call check,bonjour)
# MacPorts prefix for XDG stuff.
xdgprefix = /opt/local
else
# Otherwise we assume a linker that handles the -rpath option.
rpath = -Wl,-rpath=$(purelib)
# Link against the avahi module for Zeroconf support if we have it.
extralibs = $(call check,avahi)
endif

ifeq ($(DLL),.dll)
# Windows: executable suffix
EXE = .exe
endif

%$(DLL): %.c
	gcc $(shared) $(dllname) $(ALL_CFLAGS) $< -o $@ $(ALL_LDFLAGS)

all: $(solibs) $(targets)

# xmlzipio helper library (see xmplzipio/ for details).
xmlzipio$(DLL): $(addprefix xmlzipio/, xmlzipio.c zip.c unzip.c)
	gcc $(shared) $(dllname) $(ALL_CFLAGS) $(shell xml2-config --cflags) -DDEF_MEM_LEVEL=8 $^ -o $@ $(shell xml2-config --libs)

# We do the linking manually here, so that we can provide the appropriate
# runtime paths for the libraries (cf. rpath).

%.o: %.pure
	pure $(PIC) -c $< -o $@

# to2omm
to2omm$(EXE): to2omm.o
	g++ $< $(purelib)/pure_main.o $(rpath) $(purelib)/xml$(DLL) $(solibs) $(pure_libs) -o $@

# gosc2midi
gosc2midi$(EXE): gosc2midi.o
	g++ $< $(purelib)/pure_main.o $(rpath) $(extralibs) $(addprefix $(purelib)/, ffi$(DLL) lo$(DLL) sockets$(DLL) tk$(DLL) xml$(DLL)) $(shell pkg-config --libs libffi liblo gtk+-2.0) $(pure_libs) -o $@

clean:
	rm -Rf *.o *$(DLL)* $(targets)

# If you have pandoc, you can use these targets to create the documentation in
# man (groff), html and pdf format.
man: osc2midi-utils.1
html: README.html
pdf: README.pdf

MAN_FLAGS = -tman
HTML_FLAGS = -thtml
PDF_FLAGS  = -V "geometry:paperwidth=21cm" -V "geometry:paperheight=29.7cm" -V "geometry:vmargin=2cm" -V "geometry:hmargin=2cm" -V "fontsize:12pt"

osc2midi-utils.1: README.md
	pandoc -s -S $(MAN_FLAGS) $< | sed -e 's/ä/ae/g' -e 's/<<aggraef@gmail.com>>/<aggraef@gmail.com>/g' > $@

%.html: %.md
	pandoc -s -S $(HTML_FLAGS) $< -o $@

%.pdf: %.md
	pandoc -s -S $(PDF_FLAGS) $< -o $@

realclean: clean
	rm -f README.html README.pdf osc2midi-utils.1

# NOTE: After installation you may want to run update-desktop-database to
# add gosc2midi to your desktop environment's application menu.
install:
ifneq ($(built),)
	test -d "$(DESTDIR)$(bindir)" || mkdir -p "$(DESTDIR)$(bindir)"
	cp $(built) "$(DESTDIR)$(bindir)"
ifneq ($(findstring to2omm,$(built)),)
	test -d "$(DESTDIR)$(libdir)" || mkdir -p "$(DESTDIR)$(libdir)"
	cp $(libsrc) $(solibs) "$(DESTDIR)$(libdir)"
endif
ifneq ($(findstring gosc2midi,$(built)),)
	test -d "$(DESTDIR)$(xdgappdir)" || mkdir -p "$(DESTDIR)$(xdgappdir)"
	cp gosc2midi.desktop "$(DESTDIR)$(xdgappdir)"
	test -d "$(DESTDIR)$(xdgicondir)" || mkdir -p "$(DESTDIR)$(xdgicondir)"
	cp osc2midi.png "$(DESTDIR)$(xdgicondir)"
endif
endif

uninstall:
	rm -f $(addprefix "$(DESTDIR)$(bindir)/", $(targets))
ifneq ($(findstring to2omm,$(targets)),)
	rm -f $(addprefix "$(DESTDIR)$(libdir)/", $(libsrc) $(solibs))
endif
ifneq ($(findstring gosc2midi,$(targets)),)
	rm -f "$(DESTDIR)$(xdgappdir)/gosc2midi.desktop" "$(DESTDIR)$(xdgicondir)/osc2midi.png"
endif

DISTFILES = COPYING Makefile README.md *.c *.pure *.glade *.desktop *.png \
xmlzipio/LICENSE xmlzipio/*.c xmlzipio/*.h \
examples/Makefile examples/DJ.omm examples/Mix2L.omm examples/theremin.omm examples/*.touchosc examples/*.jzml examples/*.pd

dist:
	rm -rf $(dist)
	for x in $(dist) $(addprefix $(dist)/, examples xmlzipio); do mkdir $$x; done
	for x in $(DISTFILES); do ln -sf "$$PWD"/$$x $(dist)/$$x; done
	rm -f $(dist).tar.gz
	tar cfzh $(dist).tar.gz $(dist)
	rm -rf $(dist)

distcheck: dist
	tar xfz $(dist).tar.gz
	cd $(dist) && make && make install DESTDIR=./BUILD
	rm -rf $(dist)
