% osc2midi-utils(1)

Introduction
============

This software is for use with Spencer Jackson's [osc2midi][1] utility.
osc2midi is a flexible bidirectional OSC-MIDI bridge written in C and designed
especially for control of Jack MIDI applications from an Android device. This
is useful if you need to interface to existing MIDI software and hardware
which doesn't offer a suitable OSC interface. osc2midi was created with the
open source [Control][2] app by Charlie Roberts in mind, but it works just as
well with other OSC controller applications such as hexler's [TouchOSC][3] or
liine's [Lemur][4].

[1]: https://github.com/ssj71/OSC2MIDI
[2]: http://charlie-roberts.com/Control/
[3]: http://hexler.net/software/touchosc
[4]: https://liine.net/de/products/lemur/

TouchOSC has a graphical layout editor which lets you configure both the user
interface displayed on the device and the assignment of MIDI messages to the
control elements. To use TouchOSC as a MIDI controller, hexler provides a
special program, the TouchOSC MIDI Bridge, which needs to be run on the host
computer. This is rather convenient, but it works *only* with TouchOSC and is
proprietary software only available on Mac and Windows systems.

osc2midi and the software in this package provide you with an alternative
which covers pretty much the same functionality, but goes beyond what the
TouchOSC MIDI Bridge can do, can be used with other OSC controllers, and is
open source software which also works on Linux. (Please note that, in contrast
to the TouchOSC MIDI Bridge, the utilities in this package require that you
load the TouchOSC layout to be used, so the layout files need to be present on
*both* the device running TouchOSC and the host computer.)

The osc2midi-utils package contains the following two utilities to be used
with osc2midi and/or TouchOSC:

* **to2omm** is a little helper script which extracts the MIDI assignments
  from a TouchOSC layout (`.touchosc`) file and converts them to a
  corresponding osc2midi map (`.omm`) file ready to be used with osc2midi.
  This makes converting your TouchOSC layouts for use with osc2midi a piece of
  cake. Of course, since the created osc2midi map files are just plain text
  files, you can also edit them manually later, if you wish to do so.

* **gosc2midi** is a GTK2-based GUI frontend for osc2midi. It lets you load
  both plain midi2osc map files or TouchOSC layouts (the latter are converted
  to the former on the fly, using the to2omm program). The osc2midi map is
  shown in a text view and can be edited there, and it can be run with
  osc2midi using various configuration options.

Both programs are known to work on Linux and Mac OSX. The programs can either
be used in concert or as stand-alone utilities. If you're using TouchOSC then
you might just use to2omm to convert your existing layouts and run them with
osc2midi from the command line. You might also just use gosc2midi as a
convenient graphical frontend to osc2midi without using TouchOSC at all.

The package also includes some examples, a Makefile showing how to convert
TouchOSC layouts to corresponding osc2midi map files, and a few Pd patches for
testing purposes. Most of the examples are for use with TouchOSC right now,
but two examples for Control and the Lemur are also included, along with
corresponding test patches.

License
=======

Copyright (c) 2015 by Albert Gräf. This package is distributed under the
3-clause BSD license, please check the COPYING file for details.

Installation
============

You need:

- [Pure](http://purelang.bitbucket.org/). Both scripts are written in the
  author's Pure programming language, so you need to have the Pure interpreter
  installed. The to2omm program also needs the pure-xml module. In addition,
  gosc2midi requires pure-ffi, pure-liblo, pure-sockets and pure-tk, as well
  as pure-avahi (on Linux) or pure-bonjour (on the Mac) if you want/need
  Zeroconf support. You can find all of these on the Pure website.

- [Gnocl](http://www.gnocl.org), the Tcl/GTK bridge, and the GTK2 libraries.
  You'll also need the GnoclBuilder and GnoclGconf modules. These are required
  by gosc2midi in addition to the Pure modules.

- [osc2midi](https://github.com/ssj71/OSC2MIDI) to read the map files created
  with to2omm and perform the actual OSC-MIDI translation. Please check the
  given link for instructions on how to obtain and install this software.
  We recommend using osc2midi 0.2.3 or later.

Most of the requisite software should be readily available in the package
repositories of your Linux distribution. In particular, users of Arch Linux
will find osc2midi, osc2midi-utils and all required dependencies in the [Arch
User Repositories][5]. Thus, if you're running Arch then you can install
everything that's needed simply as follows:

    yaourt -S osc2midi-utils

Alternatively, you may want to install the current development versions of
both packages, so that you have all the latest bugfixes:

    yaourt -S osc2midi-git osc2midi-utils-git

Debian/Ubuntu packages for most of the dependencies are available on
[Launchpad][6]. Mac OS X users will find suitable packages in [MacPorts][7]
(please check the [Pure On Mac OS X][8] wiki page for details).

[5]: https://aur.archlinux.org/
[6]: https://launchpad.net/~dr-graef
[7]: http://www.macports.org/
[8]: https://bitbucket.org/purelang/pure-lang/wiki/PureOnMacOSX

To compile the software, check the included Makefile for settings that might
need to be adjusted for your system, then run:

    make
    sudo make install

It's also possible to build the programs separately, by using the `progs` make
variable. In particular, if your system lacks gosc2midi's dependencies, then
you can still build and install just to2omm as follows:

    make progs=to2omm
    sudo make install

The installation prefix can be set with the `prefix` make variable. E.g., to
install under `/usr` instead of the default `/usr/local`:

    sudo make install prefix=/usr

Package maintainers can also employ the customary `DESTDIR` variable for
staged installations:

    sudo make install prefix=/usr DESTDIR=/tmp/staged-root

The package also contains an application icon and desktop file for the
gosc2midi program which can be used with Linux desktop environments such as
Gnome and KDE. These should be installed to the appropriate locations, but you
may have to run `update-desktop-database` afterwards to have gosc2midi show up
in the menus.

If you have [pandoc](http://pandoc.org/) installed, then you can produce a
manual in man (groff), html or pdf format from the README file using the
appropriate make targets:

    make man
    make html
    make pdf

OSC Controllers
===============

In addition, you'll also need the OSC controller software that you want to
use. There are various apps on both Android and iOS that you might give a try.
I'd recommend to at least have a look at Charlie Roberts' [Control][2],
hexler's [TouchOSC][3] and liine's [Lemur][4]. These are all available in the
Google Play and iTunes stores.

**Control** has the advantage that it's open-source software and that it's
extensible using JavaScript, which makes it a lot of fun for programmers, but
somewhat harder to use for novices. It also has a few bugs (at least that's my
experience with the Android version, YMMV), and active development seems to
have stopped some time in 2013. But as Control is free (as in beer) you might
just as well give it a try, and if you're a programmer then you might also
grab the source code and build your own version if needed.

The **Lemur** is the descendent of JazzMutant's venerable Lemur hardware
controller which is the granddaddy of all multi-touch OSC devices. It's a bit
on the expensive side, but many still consider it the head of the pack because
of its many features. The Lemur offers a wide range of interesting control
elements, physical models and comprehensive scripting capabilities, and it
comes with a graphical editor which lets you create interfaces easily and
quickly. It also has its rough edges, however, in particular on Android (at
the time of this writing, it lacks support for immersive mode, only works in
landscape orientation and, unlike TouchOSC, doesn't automatically adjust to
different device resolutions). At present, gosc2midi doesn't provide any
built-in support for Lemur template files, but we'll probably look into that
in the future.

**TouchOSC** isn't quite as capable as Control or the Lemur, but it works very
well, looks nice and isn't expensive. It also has a graphical editor for
creating your own interfaces, which makes it very easy to use. TouchOSC's only
real shortcoming is that it has no scripting capabilities at all, so any
complex behaviors of the control elements require interaction with the
host. But for simple interfaces TouchOSC does the job very well, and gosc2midi
has built-in support for it, so chances are that your favorite layouts will
just work without much ado.

to2omm
======

After installing the program, just run to2omm with the target TouchOSC layout
file from the command line, e.g.:

    to2omm twofaders.touchosc

This will print the converted osc2midi map on stdout, so that you can quickly
check the MIDI assignments recognized by the program. To create the actual
`.omm` file, you can redirect the output to a file as usual:

    to2omm twofaders.touchosc > twofaders.omm

After that you invoke osc2midi on the generated map as follows:

    osc2midi -m twofaders.omm

Or you can simply pipe to2omm's output directly into osc2midi, so that you
don't have to create the intermediate .omm file (note that the dash at the end
of the command tells osc2midi to read the map file from stdin):

    to2omm twofaders.touchosc | osc2midi -m -

Please check the oscmidi documentation for other options provided to set up
the OSC ports, etc., as needed. For instance:

    osc2midi -p 8000 -a myphone:9000 -m twofaders.omm

This uses the standard TouchOSC ports and sets up the MIDI->OSC connection.
To make this work, replace `myphone` with the hostname or IP address of the
device running TouchOSC, or use the broadcast address `255.255.255.255`
instead.

In addition, to make the OSC->MIDI part of the connection work, you'll have to
configure TouchOSC so that it sends OSC data to the computer running osc2midi.
To these ends, from TouchOSC's main settings screen, go to the `OSC`
configuration screen and enter the computer's hostname (or IP address) in the
`Host` field. Also, make sure that the port number given in the `Port
(outgoing)` field matches the value of osc2midi's incoming port (`-p` option).

You'll also have to establish the Jack MIDI connections between osc2midi and
the MIDI client. This is most conveniently done using a Jack patchbay such as
[QjackCtl][9]. Note that on Linux, for clients like Pd which don't support
Jack MIDI in a direct manner, you'll have to use a Jack-ALSA bridge like
[a2jmidid][10] which makes the ALSA MIDI ports accessible in Jack MIDI. On Mac
OS X, running [JackOSX][11], you can achieve the same by connecting osc2midi to
some virtual MIDI devices created using the [IAC driver][12].

[9]:  http://qjackctl.sourceforge.net/
[10]: http://home.gna.org/a2jmidid/
[11]: http://www.jackosx.com/
[12]: http://www.soundonsound.com/sos/nov08/articles/applenotes_1108.htm

For testing purposes, it may be useful to add the `-v` option so that osc2midi
prints some information about its configuration as well as OSC and MIDI
messages it receives along with the corresponding conversions:

    osc2midi -v -p 8000 -a myphone:9000 -m twofaders.omm

Note that the gosc2midi program makes all of this much easier. It also
provides Zeroconf support for osc2midi applications and allows you to detect
your OSC devices.

Conversion Options
==================

The to2omm program can also be invoked with the `-h` (or `--help`) option to
print a short help message.

Right now, there's only one other option, `-c` (or `--compat`) which enforces
a special mode for compatibility with the TouchOSC MIDI Bridge.

This option only affects the treatment of the special realtime MIDI messages
(start, stop and continue). The TouchOSC editor lets you assign these either
to the primary variable of a button or toggle control, or to the touch (z)
variable associated with any other control.

The `-c` option has the effect that these messages will be triggered whenever
the corresponding variable changes status, i.e., when it goes to either on or
off. This matches the behavior of the TouchOSC MIDI bridge, but is rather
counter-intuitive in many cases since the corresponding MIDI message will then
be triggered *twice* when the control goes on and then back off again.

In contrast, the default behavior of maps generated with to2omm is to only
trigger these messages when the control goes on. This seems to be appropriate
for most use cases, but you can specify the `-c` option if strict
compatibility with the TouchOSC MIDI bridge is required.

Please note that in either case, osc2midi will not keep track of the current
on/off status of controls associated with these messages, so *sending* these
MIDI messages will not have the desired effect of toggling the control's
status as displayed by the TouchOSC application. If this is a problem then
it's better to map the control to a different kind of MIDI message carrying
the actual value of the control, such as MIDI controller or note messages.

gosc2midi
=========

The screenshot below shows the gosc2midi main window, which contains a text
editor with the loaded map file, another text view with the messages printed
by osc2midi (you can switch between the two views with the tabs at the top),
and a strip with various buttons and control elements which let you load and
run a map file, and change various option values.

![screenshot](screenshot.png)

The buttons with the main functions of the program are:

* **New**: Create a new (and empty) map.

* **Open**: Open an existing map file and display it in the editor (`Map`
  view). You can also load a TouchOSC layout instead, which will be converted
  to the osc2midi map format on the fly.

* **Save**: Save the map shown in the editor as an osc2midi map file. You are
  prompted for a file name (with the current file name as the default, if
  any). If the map was originally loaded from a TouchOSC layout, you are given
  the option to save it under the same name with new extension .omm.

* **Execute**: Runs the map shown in the editor with osc2midi. Messages from
  osc2midi are shown in the `Output` view.

* **Quit**: Exit the program. The currently loaded file and the various option
  values will be remembered across invocations.

Note that it is possible to edit the map file in the editor. osc2midi will be
run with whatever map is in the editor widget at the time you hit the
**Execute** button. But the modified map will not be saved unless you tell
gosc2midi to do so with the **Save** button.

The available options are (these are all remembered across invocations):

* **Incoming Port**: The UDP port on which osc2midi receives OSC messages.
  This value is the same as osc2midi's `-p` option, and should match the
  *outgoing* port of your OSC application. (In addition, you'll also have to
  configure your OSC application so that it sends OSC data to the computer
  running gosc2midi.)

* **Outgoing**, **Port**: The IP address (or hostname) and the UDP port of the
  target device running the OSC application. This only needs to be specified
  when **MIDI->OSC** is enabled (see below). Both values are passed using
  osc2midi's `-a` option, which is used to send OSC data back to the device in
  reverse (MIDI -> OSC) mappings. The given port number should match the
  *incoming* port of your OSC application.

* **?**: This little button next to the **Outgoing** field can be used to have
  gosc2midi auto-detect the address of your OSC device. A little dialog will
  be shown which explains the process and also displays the address and port
  of the host computer to connect to. Once you have your OSC application set
  up so that it transmits OSC messages to gosc2midi, just hit the **Ok**
  button. You then have 5 seconds to trigger one of the controls to have your
  device transmit a message. After receiving the message, gosc2midi determines
  its source address and fills in the **Outgoing** field accordingly. (You'll
  still have to enter the right value for the **Port** field manually,
  though.)

* **Verbose**: Prints extra status information and echoes matched OSC->MIDI
  and MIDI->OSC rules on stdout (osc2midi's `-v` option). This option may
  generate a *lot* of output, so you probably want to turn it off during live
  performances, but it is indispensable while you're still debugging a map.

* **OSC->MIDI**, **MIDI->OSC**: Enables or disables OSC->MIDI and MIDI->OSC
  mappings, respectively. Both are enabled by default. If only one of these is
  enabled, osc2midi is passed the corresponding option (`-o2m`, `-m2o`). If
  neither option is enabled, osc2midi is passed the `-mon` option which
  enables monitor mode, in which osc2midi only prints received OSC messages
  (this is useful if you're trying to figure out which OSC messages are
  emitted by the device).

* **Strict**: Enables or disables "strict" mode of matching conversion rules,
  in which osc2midi verifies the consistency of variable bindings if a
  variable occurs twice in an OSC or MIDI pattern. (Please check the osc2midi
  documentation for details.)

Note that changing any of these options will only have an effect after
restarting osc2midi using the **Execute** button.

Usage
=====

You can run `gosc2midi -h` to get a brief overview of the command line syntax.

Normally you just run `gosc2midi` without any arguments. Optionally you may
specify a file name on the command line, which indicates the osc2midi map file
or TouchOSC layout to be loaded on startup. (Note that you have to specify a
full file name there, including path and the `.omm` or `.touchosc` extension;
unlike osc2midi, gosc2midi won't perform a search for the map file. If you
have installed the desktop icon, then you can also simply drag an osc2midi map
or TouchOSC layout file from the file manager to the icon to launch gosc2midi
with the corresponding file.)

A typical workflow with gosc2midi goes like this:

1. Push **Open** to load the osc2midi map or TouchOSC layout file that you
   want to use, or **New** to create a new one. Optionally, edit the map as
   needed (and don't forget to hit **Save** to save your edits, if you'd like
   to keep them for later).

2. To set up the network connections, push the **?** button next to the
   **Outgoing** field and note the IP address and port number displayed in the
   dialog. Launch your OSC application and make sure that its outgoing OSC
   connection is set to the given IP address and port number. Then push **Ok**
   to start the auto-detection and trigger one of the controls in the OSC
   app. The IP address in the **Outgoing** field of gosc2midi should now be
   set to the address of your OSC device. Finally, check that the port number
   in the **Port** field matches the incoming port of your OSC application.

3. Push **Execute** to run osc2midi. This automatically switches to the
   `Output` tab where messages from osc2midi are shown. In any case, you'll
   see the command with which osc2midi was invoked, the OSC port on which
   osc2midi is listening, and the address of the outgoing OSC connection (if
   any). If verbose mode is on, osc2midi will also print various additional
   status information, including the parsed map rules, and, once osc2midi
   starts receiving OSC and MIDI input, the converted messages.

4. Set up Jack MIDI connections between osc2midi and the MIDI client using,
   e.g., QJackctl. Note that you can use QJackctl's patchbay to have this done
   automatically whenever osc2midi and the MIDI client are running. Also note
   that QJackctl can automatically launch a2jmidid when starting up Jack, in
   order to connect with ALSA MIDI clients. To these ends, open QJackctl's
   setup dialog and enter the command `a2jmidid -u &` into the
   `Execute script after startup` field on the `Options` tab.

5. Use **Quit** (or close the gosc2midi window) to exit gosc2midi when you're
   done. This will also stop osc2midi if it is running.

Step 2 may fail if gosc2midi doesn't receive an OSC message within 5 seconds
after pushing the **Ok** button. In this case make sure that you have the OSC
connections configured properly on the device and try again. If that doesn't
help then most likely there's a network connection issue such as a firewall
blocking the ports that are used for the OSC connections.

Step 2 can be skipped if you're only using the **OSC->MIDI** mapping. Both
steps 1 and 2 can be repeated as many times as needed, but note that you'll
have to restart osc2midi with the **Execute** button each time that you change
map file or options, or invoke the auto-detection.

Step 2 can also be skipped if you just broadcast the outgoing OSC messages to
your local network. To these ends, enter `255.255.255.255` (or the broadcast
address of the local subnet that you're using) as the outgoing address. Note
that to make this work, all connected devices will have to listen on the same
port.

You can also run multiple instances of gosc2midi on the same computer, e.g.,
to connect with multiple OSC devices requiring different configurations and/or
map files. To make this work, give each instance a unique incoming port
number. This can be done either by editing the **Incoming Port** field, or by
using the `-p` command line option, e.g.: `gosc2midi -p 8001`. Likewise,
outgoing connections can be configured on the command line with the `-a`
option as follows: `gosc2midi -a myphone:4711`. gosc2midi's `-c` option also
allows you to maintain different alternate setups in the gconf database, see
below.

Maintaining Alternate Configurations
====================================

As mentioned previously, gosc2midi automatically remembers all option values
and the name of the loaded map file across invocations. Thus, once you've
configured everything to your liking, just running `gosc2midi` without any
arguments will reload the same setup, so that you can skip steps 1 and 2 from
the previous section and be ready to go immediately.

gosc2midi also allows you to maintain different alternate setups. To these
ends, you can run gosc2midi with the `-c` (`--config`) option, specifying the
desired name of the configuration as the argument of the `-c` option. For
instance:

    gosc2midi -c touchosc

Then just set up everything as needed and exit gosc2midi. Next time you invoke
gosc2midi with the same configuration name, your setup will be restored to
what it was before. You can also list the available configurations with
`gosc2midi -l`, and get rid of obsolete configurations with `gosc2midi -d
config-name`.

gosc2midi stores all configuration data in the Gnome gconf database, so
Gnocl's gconf module is required to make all this work. On Linux and Mac OS X,
this data is normally kept under `~/.config/gconf/apps/gosc2midi`, copying
this directory should be all that's needed to move your gosc2midi
configuration data to another computer.

Zeroconf Support
================

gosc2midi provides some rudimentary support for Zeroconf (Avahi/Bonjour) if it
is available. To make this work, you need to have the pure-avahi module
(pure-bonjour on the Mac) installed, and the Avahi/Bonjour service needs to
run on your computer.

This makes gosc2midi publish `osc2midi` as an OSC/UDP service while it runs.
(If multiple instances of gosc2midi are running in your local network, they
will be named `osc2midi`, `osc2midi #2`, `osc2midi #3`, etc.) If your OSC
device supports Zeroconf then you can connect to this service to have the
outgoing address and port configured accordingly. This makes setting up the
network connections on the device easier, but note that you'll still have to
configure the OSC back connection from osc2midi to your device if it is
needed.

Zeroconf is known to work with Control and TouchOSC, while the Lemur only
allows you to set the addresses manually or via its editor application.
Depending on your local network environment, Zeroconf can sometimes be flaky.
It may report stale addresses or none at all. If the osc2midi service isn't
shown on the device, make sure that you have the Avahi/Bonjour daemon running
on the host machine, and that the host and the device are connected to the
same subnet. If all else fails, use the IP address and port number reported by
gosc2midi's setup dialog (**?** button) and configure the connection manually.

Feedback and Bug Reports
========================

As usual, bug reports, patches, feature requests, other comments and source
contributions are more than welcome. Just drop me an email, file an issue at
the tracker or send me a pull request on osc2midi-util's Bitbucket page
<https://bitbucket.org/agraef/osc2midi-utils>.

Enjoy! :)

Albert Gräf <aggraef@gmail.com>
